<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>Haz clic derecho para editar la dirección o la etiqueta</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Crea una nueva direccióon</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>&amp;Nuevo</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Copia la direccón seleccionada al portapapeles del sistema</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copiar</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation>C&amp;errar</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>&amp;Copiar Direccón</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Elimina la dirección seleccionada de la lista</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Exporta los datos de la pestaña actual a un archivo</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>&amp;Exportar</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>&amp;Eliminar</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>Elige la dirección a la que enviar las monedas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Elige la direccón con la que recibir monedas</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>E&amp;legir</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>Enviando direcciones</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>Recibiendo direcciones</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>Estas son tus direcciones de Nexa para enviar pagos. Comprueba siempre la cantidad y la dirección receptora antes de enviar monedas.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>Estas son tus direcciones de Nexa para recibir pagos. Se recomienda usar una nueva dirección receptora para cada transacción</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>Copiar &amp;Etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>Exportar Lista de Direcciones</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Archivo separado por comas (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>Exportacón Fallida</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>Ha ocurrido un error intentando guardar la lista de direcciones en %1. Por favor intentalo de nuevo.</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(sin etiqueta)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Dialogo de Contraseña</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Introduzca la contraseña</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Nueva contraseña</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Repite la nueva contraseña</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>Encriptar cartera</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>Esta operacón necesita tu contraseña de la cartera para desbloquear la cartera.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Desbloquear cartera</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>Esta operación requiere tu contraseña de la cartera para desencriptar la cartera.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>Desencriptar cartera</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Cambiar contraseña</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Confirm wallet encryption</source>
        <translation>Confirmar encriptación de la cartera</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>Advertencia: Si encriptas tu cartera y pierdes tu contraseña, &lt;b&gt;PERDERÁS TODOS TUS NEXA COINS&lt;/B&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Estás seguro ue deseas encriptar tu cartera?</translation>
    </message>
    <message>
        <location line="+119"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>Advertencia: La Tecla de Bloqueo de Mayusculas esta activada!</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>Cartera encriptada</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Introduzca la nueva contraseña de la cartera. &lt;br/&gt;Por favor utilice una contraseña de &lt;b&gt;diez o mas caracteres aleatorios&lt;/b&gt;, o &lt;b&gt;ocho o mas palabras&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>Introduzca la antigua contraseña y la nueva contraseña en la cartera.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons you should no longer keep previous unencrypted backups as your funds will be at risk if someone gains access to them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>Encriptación de la cartera fallida</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>La encriptación de la cartera ha fallado debido a un error interno. Tu cartera no ha sido encriptada.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>Las contraseñas proporcianadas no se corresponden.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>Desbloqueo de la cartera fallido</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>La contraseña introducida para desencriptar la cartera es incorrecta.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>Desencriptación de la cartera fallida</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>Contraseña de la cartera cambiada correctamente</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation type="unfinished">User Agent</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+351"/>
        <source>Sign &amp;message...</source>
        <translation>Firmar &amp;mensaje...</translation>
    </message>
    <message>
        <location line="+435"/>
        <source>Synchronizing with network...</source>
        <translation>Sincronizando con la red...</translation>
    </message>
    <message>
        <location line="-556"/>
        <source>&amp;Overview</source>
        <translation>&amp;Vista general</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>Nodo</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>Mostrar vista general de la cartera</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>&amp;Transacciones</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>Navegar historial de transacciones</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse or Send Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Token History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Token History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>E&amp;xit</source>
        <translation>S&amp;alir</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>Salir de la aplicación</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>Acerca de &amp;Qt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Muestra información acerca de Qt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>&amp;Opciones...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>&amp;Encriptar Cartera...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>&amp;Hacer copia de seguridad de la cartera...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Restore Wallet...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Restore wallet from another location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Passphrase...</source>
        <translation>&amp;Cambiar contraseña...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sending addresses...</source>
        <translation>&amp;Enviando direcciones...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>&amp;Recibiendo direcciones..</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>Abrir &amp;URI...</translation>
    </message>
    <message>
        <location line="+724"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-303"/>
        <source>Importing blocks from disk...</source>
        <translation>Importando bloques desde disco...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>Reindexando bloques en el disco...</translation>
    </message>
    <message>
        <location line="-554"/>
        <source>Send coins to a Nexa address</source>
        <translation>Envia monedas a una dirección Nexa</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Backup wallet to another location</source>
        <translation>Copia de seguridad del monedero en otra ubicación</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>Cambiar la contraseña utilizada para el cifrado del monedero</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>&amp;Ventana de depuración</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>Abrir la consola de depuración y diagnóstico</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>&amp;Verificar mensaje...</translation>
    </message>
    <message>
        <location line="+522"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-785"/>
        <source>Wallet</source>
        <translation>Monedero</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>&amp;Enviar</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>&amp;Recibir</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>&amp;Show / Hide</source>
        <translation>&amp;Mostrar / Ocultar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>Mostrar u ocultar la ventana principal</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>Cifrar las claves privadas de su monedero</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>Firmar mensajes con sus direcciones Nexa para demostrar la propiedad</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>Verificar mensajes comprobando que están firmados con direcciones Nexa concretas</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Open a %1: URI or payment request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configuración</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>Barra de pestañas</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>Mostrar la lista de direcciones de envío y etiquetas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>Muestra la lista de direcciones de recepción y etiquetas</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Command-line options</source>
        <translation>&amp;Opciones de consola de comandos</translation>
    </message>
    <message numerus="yes">
        <location line="+390"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n conexión activa hacia la red Nexa</numerusform>
            <numerusform>%n conexiones activas hacia la red Nexa</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+43"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>%n bloque procesado del historial de transacciones.</numerusform>
            <numerusform>%n bloques procesados del historial de transacciones.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+27"/>
        <source>%1 behind</source>
        <translation>%1 atrás</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>El último bloque recibido fue generado hace %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>Las transacciones posteriores aún no están visibles.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>Información</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Up to date</source>
        <translation>Actualizado</translation>
    </message>
    <message>
        <location line="-437"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation>Mostrar el mensaje de ayuda %1 para obtener una lista de los posibles comandos de linea de comandos de Nexa</translation>
    </message>
    <message>
        <location line="+194"/>
        <source>%1 client</source>
        <translation>%1 cliente</translation>
    </message>
    <message>
        <location line="+227"/>
        <source>No block source available...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Catching up...</source>
        <translation>Actualizando...</translation>
    </message>
    <message>
        <location line="+164"/>
        <source>Date: %1
</source>
        <translation>Fecha: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>Amount: %1
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>Tipo: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>Etiqueta: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>Dirección: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>Transacción enviada</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>Transacción entrante</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>El monedero está &lt;b&gt;cifrado&lt;/b&gt; y actualmente &lt;b&gt;desbloqueado&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation type="unfinished">Copiar ubicación</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation type="unfinished">Copiar capa</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation type="unfinished">Copiar cantidad</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation type="unfinished">Copiar ID de transacción</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation type="unfinished">Copiar cantidad</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation type="unfinished">Copiar cuota</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation type="unfinished">Copiar después de couta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation type="unfinished">Copiar bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation type="unfinished">Copiar polvo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation type="unfinished">Copiar cambio</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation type="unfinished">ninguna</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(sin etiqueta)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation type="unfinished">Cantidad:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation type="unfinished">Bytes:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation type="unfinished">Cuantía:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation type="unfinished">Tasa:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation type="unfinished">Polvo:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation type="unfinished">Después de tasas:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation type="unfinished">Cambio:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation type="unfinished">Cantidad</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation type="unfinished">Fecha</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation type="unfinished">Confirmado</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Address</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+61"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not unlock wallet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished">Formulario</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation type="unfinished">Hora del último bloque</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation type="unfinished">Ocultar</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>&amp;Principal</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation>Iniciar automaticamente  %1 al encender el sistema.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation>&amp;Iniciar %1 al iniciar el sistema</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Number of script &amp;verification threads</source>
        <translation>Número de hilos de &amp;verificación de scripts</translation>
    </message>
    <message>
        <location line="+211"/>
        <source>Allow incoming connections</source>
        <translation>Aceptar conexiones entrantes</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>Dirección IP del proxy (p. ej. IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="-118"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+156"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>Minimizar en lugar de salir de la aplicación cuando la ventana está cerrada. Cuando se activa esta opción, la aplicación sólo se cerrará después de seleccionar Salir en el menú.</translation>
    </message>
    <message>
        <location line="+80"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>Identificadores URL de terceros (por ejemplo, un explorador de bloques) que aparecen en la pestaña de transacciones como elementos del menú contextual. El %s en la URL es reemplazado por el valor hash de la transacción. Se pueden separar URL múltiples por una barra vertical |.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>Identificadores URL de transacciones de terceros</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>Opciones activas de consola de comandos que tienen preferencia sobre las opciones anteriores:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>Restablecer todas las opciones predeterminadas del cliente.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Restablecer opciones</translation>
    </message>
    <message>
        <location line="-504"/>
        <source>&amp;Network</source>
        <translation>&amp;Red</translation>
    </message>
    <message>
        <location line="-220"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = automático, &lt;0 = dejar libres ese número de núcleos)</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>W&amp;allet</source>
        <translation>&amp;Monedero</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>Experto</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable coin &amp;control features</source>
        <translation>Habilitar funcionalidad de &amp;coin control</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>Si desactiva el gasto del cambio no confirmado, no se podrá usar el cambio de una transacción hasta que se alcance al menos una confirmación. Esto afecta también a cómo se calcula su saldo.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>&amp;Gastar cambio no confirmado</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&amp;Rescan wallet on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>Abrir automáticamente el puerto del cliente Nexa en el router. Esta opción solo funciona si el router admite UPnP y está activado.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>Mapear el puerto mediante &amp;UPnP</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>Conectarse a la red Nexa a través de un proxy SOCKS5.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>&amp;Conectarse a través de proxy SOCKS5 (proxy predeterminado):</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>Dirección &amp;IP del proxy:</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>&amp;Puerto:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>Puerto del servidor proxy (ej. 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>Usado para alcanzar compañeros via:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>Conectar a la red Nexa mediante un proxy SOCKS5 por separado para los servicios ocultos de Tor.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Usar distintos proxys SOCKS5 para comunicarse vía Tor de forma anónima:</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>&amp;Ventana</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>Minimizar la ventana a la bandeja de iconos del sistema.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>&amp;Minimizar a la bandeja en vez de a la barra de tareas</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>M&amp;inimize on close</source>
        <translation>M&amp;inimizar al cerrar</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>&amp;Interfaz</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>I&amp;dioma de la interfaz de usuario</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation>El idioma de la interfaz de usuario puede establecerse aquí. Esta configuración tendrá efecto tras reiniciar %1.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>Mostrar las cantidades en la &amp;unidad:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>Elegir la subdivisión predeterminada para mostrar cantidades en la interfaz y cuando se envían coins.</translation>
    </message>
    <message>
        <location line="-502"/>
        <source>Whether to show coin control features or not.</source>
        <translation>Mostrar o no funcionalidad de Coin Control</translation>
    </message>
    <message>
        <location line="-33"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a full blockchain resynchronization on the next startup (one time only).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Resynchronize block data on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+694"/>
        <source>&amp;OK</source>
        <translation>&amp;Aceptar</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+124"/>
        <source>default</source>
        <translation>predeterminado</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>none</source>
        <translation>ninguna</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Confirm options reset</source>
        <translation>Confirme el restablecimiento de las opciones</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>Se necesita reiniciar el cliente para activar los cambios.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>El cliente se cerrará. ¿Desea continuar?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>Este cambio exige el reinicio del cliente.</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>La dirección proxy indicada es inválida.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>La información mostrada puede estar desactualizada. Su monedero se sincroniza automáticamente con la red Nexa después de que se haya establecido una conexión, pero este proceso aún no se ha completado.</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>Watch-only:</source>
        <translation>De observación:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>Disponible:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>Su saldo disponible actual</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Pending:</source>
        <translation>Pendiente:</translation>
    </message>
    <message>
        <location line="-236"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>Total de transacciones pendientes de confirmar y que aún no contribuye al saldo disponible</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>No madurado:</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>Saldo recién minado que aún no ha madurado.</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Balances</source>
        <translation>Saldos</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Total:</source>
        <translation>Total:</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>Su saldo actual total</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>Su saldo actual en direcciones watch-only</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Spendable:</source>
        <translation>Gastable:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>Transacciones recientes</translation>
    </message>
    <message>
        <location line="-317"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>Transacciones sin confirmar en direcciones watch-only</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>Saldo minado en direcciones watch-only que aún no ha madurado</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>Saldo total en las direcciones watch-only</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+375"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>Fallo en la solicitud de pago</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+95"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>Manejo de URI</translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>La búsqueda de solicitud de pago URL es válida: %1</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid payment address %1</source>
        <translation>Dirección de pago inválida %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation>URI no puede ser analizado! Esto puede ser causado por una dirección Nexa inválida o parametros URI mal formados.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>Manejo del archivo de solicitud de pago</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation>¡El archivo de solicitud de pago no puede ser leído! Esto puede ser causado por un archivo de solicitud de pago inválido.</translation>
    </message>
    <message>
        <location line="+65"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation>Solicitud de pago rechazada</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation>La red de solicitud de pago no cimbina la red cliente.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Payment request expired.</source>
        <translation>Solicitud de pago caducada.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment request is not initialized.</source>
        <translation>La solicitud de pago no se ha iniciado.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>Solicitudes de pago sin verificar a scripts de pago habitual no se soportan.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation>Solicitud de pago inválida.</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>Cantidad de pago solicitada de %1 es demasiado pequeña (considerado polvo).</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Refund from %1</source>
        <translation>Reembolsar desde %1</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation>Solicitud de pago de %1 es demasiado grande (%2 bytes, permitidos %3 bytes).</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>Fallo al comunicar con %1: %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation>¡La solicitud de pago no puede ser analizada!</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>Mala respuesta desde el servidor %1</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Network request error</source>
        <translation>Fallo de solicitud de red</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment acknowledged</source>
        <translation>Pago declarado</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+106"/>
        <source>User Agent</source>
        <translation>User Agent</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Node/Service</source>
        <translation>Nodo/Servicio</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation type="unfinished">Ping</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>Cantidad</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+832"/>
        <source>%1 d</source>
        <translation>%1 d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 m</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 s</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>N/D</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 ms</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation>%1 y %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;Guardar imagen...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>&amp;Copiar imagen</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>Guardar código QR</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>Imagen PNG (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>N/D</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>Versión del cliente</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>&amp;Información</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>Ventana de depuración</translation>
    </message>
    <message>
        <location line="+620"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location line="-587"/>
        <source>Using BerkeleyDB version</source>
        <translation>Utilizando la versión de BerkeleyDB</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Datadir</source>
        <translation>Datadir</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Startup time</source>
        <translation>Hora de inicio</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>Red</translation>
    </message>
    <message>
        <location line="-467"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>Número de conexiones</translation>
    </message>
    <message>
        <location line="+415"/>
        <source>Block chain</source>
        <translation>Cadena de bloques</translation>
    </message>
    <message>
        <location line="-408"/>
        <source>Current number of blocks</source>
        <translation>Número actual de bloques</translation>
    </message>
    <message>
        <location line="+884"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>Recibido</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>Enviado</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>&amp;Peers</source>
        <translation>&amp;Pares</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>Peers Bloqueados</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+856"/>
        <source>Select a peer to view detailed information.</source>
        <translation>Seleccionar un par para ver su información detallada.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>En la lista blanca</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Starting Block</source>
        <translation>Importando bloques...</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>Sincronizar Cabeceras</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>Bloques Sincronizados</translation>
    </message>
    <message>
        <location line="-2488"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>User Agent</translation>
    </message>
    <message>
        <location line="-2335"/>
        <source>Last block time (time since)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation>Abrir el archivo de depuración %1 desde el directorio de datos actual. Puede tardar unos segundos para ficheros de gran tamaño.</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Decrease font size</source>
        <translation>Disminuir tamaño de letra</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation>Aumentar tamaño de letra</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>&amp;Transaction Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+769"/>
        <source>Services</source>
        <translation>Servicios</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Ban Score</source>
        <translation>Puntuación de bloqueo</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>Duración de la conexión</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>Ultimo envío</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>Ultima recepción</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>La duración de un ping actualmente en proceso.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Espera de Ping</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>Desplazamiento de tiempo</translation>
    </message>
    <message>
        <location line="-2524"/>
        <source>&amp;Open</source>
        <translation>&amp;Abrir</translation>
    </message>
    <message>
        <location line="+431"/>
        <source>&amp;Console</source>
        <translation>&amp;Consola</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>&amp;Network Traffic</source>
        <translation>&amp;Tráfico de Red</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>&amp;Vaciar</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>Total:</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-530"/>
        <source>In:</source>
        <translation>Entrante:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>Saliente:</translation>
    </message>
    <message>
        <location filename="../forms/debugwindow.ui" line="-704"/>
        <source>Debug log file</source>
        <translation>Archivo de registro de depuración</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>Borrar consola</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-207"/>
        <source>&amp;Disconnect Node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;hour</source>
        <translation>1 &amp;hora</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2"/>
        <source>1 &amp;day</source>
        <translation>1 &amp;día</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 &amp;semana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 &amp;año</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation>Bienvenido a la consola RPC %1.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>Use las flechas arriba y abajo para navegar por el historial y &lt;b&gt;Control+L&lt;/b&gt; para vaciar la pantalla.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>Escriba &lt;b&gt;help&lt;/b&gt; para ver un resumen de los comandos disponibles.</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(nodo: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>via %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>nunca</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>Entrante</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>Saliente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+110"/>
        <source>&amp;Amount:</source>
        <translation>Cantidad</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&amp;Label:</source>
        <translation>&amp;Etiqueta:</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>&amp;Message:</source>
        <translation>Mensaje:</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>Reutilizar una de las direcciones previamente usadas para recibir. Reutilizar direcciones tiene problemas de seguridad y privacidad. No lo uses a menos que antes regeneres una solicitud de pago.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>R&amp;eutilizar una dirección existente para recibir (no recomendado)</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+23"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>Un mensaje opcional para adjuntar a la solicitud de pago, que se muestra cuando se abre la solicitud. Nota: El mensaje no se enviará con el pago por la red Nexa.</translation>
    </message>
    <message>
        <location line="-63"/>
        <location line="+56"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>Etiqueta opcional para asociar con la nueva dirección de recepción.</translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>Utilice este formulario para solicitar pagos. Todos los campos son &lt;b&gt;opcionales&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="-56"/>
        <location line="+7"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>Para solicitar una cantidad opcional. Deje este vacío o cero para no solicitar una cantidad específica.</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Clear all fields of the form.</source>
        <translation>Vaciar todos los campos del formulario.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Vaciar</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Requested payments history</source>
        <translation>Historial de pagos solicitados</translation>
    </message>
    <message>
        <location line="-197"/>
        <source>&amp;Request payment</source>
        <translation>&amp;Solicitar pago</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Coin freezing locks coins to make them temporarily unspendable. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Coin &amp;Freeze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>Muestra la petición seleccionada (También doble clic)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>Borrar de la lista las direcciónes actualmente seleccionadas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+53"/>
        <source>Copy URI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Copiar capa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>Copiar imagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copiar cantidad</translation>
    </message>
</context>
<context>
    <name>ReceiveFreezeDialog</name>
    <message>
        <location filename="../forms/receivefreezedialog.ui" line="+14"/>
        <source>Coin Freeze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>WARNING! Freezing coins means they will be UNSPENDABLE until the release date or block specified below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Freeze until block :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Specify the block when coins are released from the freeze. Coins are UNSPENDABLE until the freeze block.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+99"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-57"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Freeze until date and time :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Specify the future date and time when coins are released from the freeze. Coins are UNSPENDABLE until after the freeze date and time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+98"/>
        <source>&amp;Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;Aceptar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+O</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+85"/>
        <source>Copy &amp;Address</source>
        <translation>&amp;Copiar Direccón</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+87"/>
        <source>Address</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="-56"/>
        <source>QR Code</source>
        <translation>Código QR</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>Copiar &amp;URI</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Save Image...</source>
        <translation>Guardar Imagen...</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="-14"/>
        <source>Request payment to %1</source>
        <translation>Solicitar pago a %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>Información de pago</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Amount</source>
        <translation>Cantidad</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Message</source>
        <translation>Mensaje</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Freeze until</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>URI resultante demasiado grande, trate de reducir el texto de etiqueta / mensaje.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>Fallo al codificar URI en código QR.</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>(no label)</source>
        <translation>(sin etiqueta)</translation>
    </message>
    <message>
        <location line="-40"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>Mensaje</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>(no message)</source>
        <translation>(no hay mensaje)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Amount</source>
        <translation type="unfinished">Cantidad</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="+886"/>
        <source>(no label)</source>
        <translation>(sin etiqueta)</translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="-253"/>
        <source>Send Coins</source>
        <translation>Enviar coins</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>Características de Coin Control</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Select specific coins you want to use in this transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Inputs...</source>
        <translation>Entradas...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>Seleccionado automáticamente</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>Fondos insuficientes!</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>Cantidad:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>Bytes:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>Cuantía:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>Tasa:</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>After Fee:</source>
        <translation>Después de tasas:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Cambio:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>Si se marca esta opción pero la dirección de cambio está vacía o es inválida, el cambio se enviará a una nueva dirección recién generada.</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+16"/>
        <source>Custom change address</source>
        <translation>Dirección propia</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Transaction Fee:</source>
        <translation>Comisión de Transacción:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose your transaction fee.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose...</source>
        <translation>Elija...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>Colapsar ajustes de cuota</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>per kilobyte</source>
        <translation>por kilobyte</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>Si la tarifa de aduana se establece en 1000 satoshis y la transacción está a sólo 250 bytes, entonces &quot;por kilobyte&quot; sólo paga 250 satoshis de cuota, mientras que &quot;el mínimo total&quot; pagaría 1.000 satoshis. Para las transacciones más grandes que un kilobyte ambos pagan por kilobyte</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>total at least</source>
        <translation>total por lo menos</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Custom fee amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>Pagando solamente la cuota mínima es correcto, siempre y cuando haya menos volumen de transacciones que el espacio en los bloques. Pero tenga en cuenta que esto puede terminar en una transacción nunca confirmada, una vez que haya más demanda para transacciones nexa que la red pueda procesar.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(leer la sugerencia)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Use the recommended fee amount.  You can select a faster or slower confirmation time by moving the &quot;Confirmation Time&quot; slider.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Recommended:</source>
        <translation>Recomendado:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Choose a custom fee amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom:</source>
        <translation>Personalizado:</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(Tarifa inteligente no inicializado aún. Esto generalmente lleva a pocos bloques...)</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+33"/>
        <source>Transaction confirmation time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Confirmation time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>rápido</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Send to multiple recipients at once</source>
        <translation>Enviar a múltiples destinatarios de una vez</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>Añadir &amp;destinatario</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Clear all fields of the form.</source>
        <translation>Vaciar todos los campos del formulario</translation>
    </message>
    <message>
        <location line="-880"/>
        <source>Dust:</source>
        <translation>Polvo:</translation>
    </message>
    <message>
        <location line="+883"/>
        <source>Clear &amp;All</source>
        <translation>Vaciar &amp;todo</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Current Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Balance:</source>
        <translation>Saldo:</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Confirm the send action</source>
        <translation>Confirmar el envío</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>&amp;Enviar</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-565"/>
        <source>Copy quantity</source>
        <translation>Copiar cantidad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copiar cantidad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>Copiar cuota</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Copiar después de couta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Copiar bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Copiar polvo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Copiar cambio</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 a %2</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;b&gt;WARNING!!! DESTINATION IS A FREEZE ADDRESS&lt;br&gt;UNSPENDABLE UNTIL&lt;/b&gt; %1 &lt;br&gt;*************************************************&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Are you sure you want to send?</source>
        <translation>¿Seguro que quiere enviar?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>añadido como transacción de cuota</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Total Amount %1</source>
        <translation>Cantidad total %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>o</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Confirm send coins</source>
        <translation>Confirmar enviar monedas</translation>
    </message>
    <message>
        <location line="+185"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>La dirección de destinatario no es válida. Por favor revísela.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>La cantidad a pagar debe de ser mayor que 0.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>La cantidad excede su saldo.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>El total excede su saldo cuando la cuota de transacción de %1 es incluida.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>Dirección duplicada encontrada: la dirección sólo debería ser utilizada una vez por cada uso.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction creation failed!</source>
        <translation>¡Falló la creación de transacción!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>Una couta mayor que %1 se considera una cuota irracionalmente alta.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>Solicitud de pago caducada.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Pay only the required fee of %1</source>
        <translation>Pagar únicamente la cuota solicitada de %1</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>Alerta: dirección Nexa inválida</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Warning: Unknown change address</source>
        <translation>Alerta: dirección cambiada desconocida</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirm custom change address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+86"/>
        <location line="+677"/>
        <location line="+560"/>
        <source>A&amp;mount:</source>
        <translation>Ca&amp;ntidad:</translation>
    </message>
    <message>
        <location line="-1224"/>
        <source>Pay &amp;To:</source>
        <translation>&amp;Pagar a:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Choose previously used address</source>
        <translation>Escoger direcciones previamente usadas</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Private Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Dirección Nexa a la que enviar el pago</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Pegar dirección desde portapapeles</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+553"/>
        <location line="+560"/>
        <source>Remove this entry</source>
        <translation>Eliminar esta transacción</translation>
    </message>
    <message>
        <location line="-1091"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-180"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>La cuota será deducida de la cantidad que sea mandada. El destinatario recibirá menos coins de los que entres en el  </translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Amount to send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>Restar comisiones a la cantidad</translation>
    </message>
    <message>
        <location line="+643"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>Esta es una petición de pago no autentificada.</translation>
    </message>
    <message>
        <location line="+556"/>
        <source>This is an authenticated payment request.</source>
        <translation>Esta es una petición de pago autentificada.</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Un mensaje que se adjuntó a la %1 URL que será almacenada con la transacción para su referencia. Nota: Este mensaje no se envía a través de la red Nexa.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="-541"/>
        <location line="+556"/>
        <source>Pay To:</source>
        <translation>Paga a:</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+560"/>
        <source>Memo:</source>
        <translation>Memo:</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation>%1 se esta cerrando...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>No apague el equipo hasta que desaparezca esta ventana.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>Firmas - Firmar / verificar un mensaje</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>&amp;Firmar mensaje</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>Puede firmar los mensajes con sus direcciones para demostrar que las posee. Tenga cuidado de no firmar cualquier cosa de manera vaga o aleatoria, pues los ataques de phishing pueden tratar de engañarle firmando su identidad a través de ellos. Sólo firme declaraciones totalmente detalladas con las que usted esté de acuerdo.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>Dirección Nexa con la que firmar el mensaje</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>Escoger dirección previamente usada</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>Pegar dirección desde portapapeles</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>Introduzca el mensaje que desea firmar aquí</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>Firma</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>Copiar la firma actual al portapapeles del sistema</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>Firmar el mensaje para demostrar que se posee esta dirección Nexa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>Firmar &amp;mensaje</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>Vaciar todos los campos de la firma de mensaje</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>Vaciar &amp;todo</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>&amp;Verificar mensaje</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>Introduzca la dirección para la firma, el mensaje (asegurándose de copiar tal cual los saltos de línea, espacios, tabulaciones, etc.) y la firma a continuación para verificar el mensaje. Tenga cuidado de no asumir más información de lo que dice el propio mensaje firmado para evitar fraudes basados en ataques de tipo man-in-the-middle. </translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>La dirección Nexa con la que se firmó el mensaje</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>Verificar el mensaje para comprobar que fue firmado con la dirección Nexa indicada</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>Verificar &amp;mensaje</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>Vaciar todos los campos de la verificación de mensaje</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>Click en &quot;Fírmar mensaje&quot; para generar una firma</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>El desbloqueo del monedero fue cancelado.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>Mensaje firmado.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>Mensaje verificado.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnet]</translation>
    </message>
</context>
<context>
    <name>TokenDescDialog</name>
    <message>
        <location filename="../forms/tokendescdialog.ui" line="+17"/>
        <source>Token details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This pane shows a detailed description of the token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TokenHistoryView</name>
    <message>
        <location filename="../tokenhistoryview.cpp" line="+78"/>
        <location line="+19"/>
        <source>All</source>
        <translation type="unfinished">Todo</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation type="unfinished">Hoy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation type="unfinished">Esta semana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation type="unfinished">Este mes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation type="unfinished">Mes pasado</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation type="unfinished">Este año</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation type="unfinished">Rango...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation type="unfinished">Recibido con</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent</source>
        <translation type="unfinished">Enviado</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation type="unfinished">A usted mismo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation type="unfinished">Otra</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter token ID to search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation type="unfinished">Cantidad mínima</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy token id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation type="unfinished">Copiar cantidad</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation type="unfinished">Copiar transacción raw</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation type="unfinished">Mostrar detalles de la transacción</translation>
    </message>
    <message>
        <location line="+162"/>
        <source>Export Token History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation type="unfinished">Archivo separado por comas (*.csv)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation type="unfinished">Confirmado</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation type="unfinished">De observación</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation type="unfinished">Fecha</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation type="unfinished">Tipo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation type="unfinished">Etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation type="unfinished">Dirección</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation type="unfinished">Exportacón Fallida</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the token history to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation type="unfinished">Exportación finalizada</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The token history was successfully saved to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Range:</source>
        <translation type="unfinished">Rango:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation type="unfinished">para</translation>
    </message>
</context>
<context>
    <name>TokenTableModel</name>
    <message>
        <location filename="../tokentablemodel.cpp" line="+244"/>
        <source>Date</source>
        <translation type="unfinished">Fecha</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation type="unfinished">Tipo</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Net Amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+49"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation type="unfinished">Abierto hasta %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation type="unfinished">Sin conexion</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation type="unfinished">Sin confirmar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation type="unfinished">Confirmando (%1 de %2 confirmaciones recomendadas)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation type="unfinished">Confirmado (%1 confirmaciones)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation type="unfinished">En conflicto</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation type="unfinished">Abandonado</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation type="unfinished">No disponible (%1 confirmaciones. Estarán disponibles al cabo de %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation type="unfinished">Este bloque no ha sido recibido por otros nodos y probablemente no sea aceptado!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation type="unfinished">Generado pero no aceptado</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation type="unfinished">Recibido con</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation type="unfinished">Recibidos de</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent</source>
        <translation type="unfinished">Enviado</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation type="unfinished">Pago proprio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Melt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation type="unfinished">Otra</translation>
    </message>
    <message>
        <location line="+323"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation type="unfinished">Estado de transacción. Pasa el ratón sobre este campo para ver el número de confirmaciones.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation type="unfinished">Fecha y hora en que se recibió la transacción.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation type="unfinished">Tipo de transacción.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation type="unfinished">Si una dirección watch-only está involucrada en esta transacción o no.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation type="unfinished">Descripción de la transacción definido por el usuario.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation type="unfinished">Cantidad retirada o añadida al saldo.</translation>
    </message>
</context>
<context>
    <name>TokensViewDialog</name>
    <message>
        <location filename="../forms/tokensviewdialog.ui" line="+14"/>
        <source>Tokens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Amount: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of tokens to send&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>  Pay To: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The Nexa address to send the payment to</source>
        <translation type="unfinished">Dirección Nexa a la que enviar el pago</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+A</source>
        <translation type="unfinished">Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation type="unfinished">Pegar dirección desde portapapeles</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+P</source>
        <translation type="unfinished">Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove this entry</source>
        <translation type="unfinished">Eliminar esta transacción</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Send tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The unique token identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-45"/>
        <source>The token group identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Name</source>
        <translation type="unfinished">Nombre</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The name of the token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The token ticker symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed token balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed token balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If checked then this item is a subgroup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tokensviewdialog.cpp" line="+367"/>
        <location line="+7"/>
        <location line="+4"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-11"/>
        <source>string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NaN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+84"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Total Mintage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+94"/>
        <source>Token mintage is unavailable because the database needs a reindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+43"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Ticker:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+43"/>
        <source>URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Hash:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-39"/>
        <location line="+43"/>
        <source>Decimals:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+14"/>
        <source>Current Authorities:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-88"/>
        <location line="+76"/>
        <source>Mint:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Melt:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Renew:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Rescript:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Subgroup:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+76"/>
        <source>Token authorities are unavailable because the database needs a reindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+77"/>
        <source>Balance:</source>
        <translation type="unfinished">Saldo:</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+77"/>
        <source>Pending:</source>
        <translation type="unfinished">Pendiente:</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Are you sure you want to send?</source>
        <translation type="unfinished">¿Seguro que quiere enviar?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;b&gt;Token(s)&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Confirm send tokens</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+36"/>
        <source>Open until %1</source>
        <translation>Abierto hasta %1</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1/offline</source>
        <translation>%1/sin conexión</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>abandoned</source>
        <translation>abandonado</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%1/unconfirmed</source>
        <translation>%1/no confirmado</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>confirmaciones %1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, no ha sido emitido con éxito aún</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>Generado</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+128"/>
        <source>From</source>
        <translation>Desde</translation>
    </message>
    <message>
        <location line="-128"/>
        <source>unknown</source>
        <translation>desconocido</translation>
    </message>
    <message>
        <location line="-20"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+114"/>
        <source>To</source>
        <translation>Para</translation>
    </message>
    <message numerus="yes">
        <location line="-223"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+8"/>
        <source>double spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>conflicted</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+30"/>
        <source>, broadcast through %n node(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+38"/>
        <source>change address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>dirección propia</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+119"/>
        <source>watch-only</source>
        <translation>de observación</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+28"/>
        <location line="+113"/>
        <source>label</source>
        <translation>etiqueta</translation>
    </message>
    <message>
        <location line="-122"/>
        <location line="+63"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-167"/>
        <source>Freeze until</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+213"/>
        <source>Credit</source>
        <translation>Credito</translation>
    </message>
    <message numerus="yes">
        <location line="-361"/>
        <source>matures in %n more block(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>no aceptada</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+218"/>
        <source>Debit</source>
        <translation>Enviado</translation>
    </message>
    <message>
        <location line="-237"/>
        <source>Total debit</source>
        <translation>Total enviado</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>Total recibido</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>Comisión de transacción</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>Cantidad neta</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+12"/>
        <source>Message</source>
        <translation>Mensaje</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction Idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>Vendedor</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>Los coins generados deben madurar %1 bloques antes de que puedan gastarse. Cuando generó este bloque, se transmitió a la red para que se añadiera a la cadena de bloques. Si no consigue entrar en la cadena, su estado cambiará a &quot;no aceptado&quot; y ya no se podrá gastar. Esto puede ocurrir ocasionalmente si otro nodo genera un bloque a pocos segundos del suyo.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Token ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-50"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Ticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-54"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Name</source>
        <translation type="unfinished">Nombre</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Decimals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-53"/>
        <source>Melt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+34"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Amount Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Amount Received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Debug information</source>
        <translation>Información de depuración</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>Transacción</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>entradas</translation>
    </message>
    <message>
        <location line="-95"/>
        <location line="+28"/>
        <location line="+34"/>
        <location line="+55"/>
        <source>Amount</source>
        <translation>Cantidad</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>verdadero</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>falso</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>Esta ventana muestra información detallada sobre la transacción</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Open until %1</source>
        <translation>Abierto hasta %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>Sin conexion</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Sin confirmar</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Abandoned</source>
        <translation>Abandonado</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Confirmando (%1 de %2 confirmaciones recomendadas)</translation>
    </message>
    <message>
        <location line="-72"/>
        <source>Address or Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Confirmado (%1 confirmaciones)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>En conflicto</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>No disponible (%1 confirmaciones. Estarán disponibles al cabo de %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Este bloque no ha sido recibido por otros nodos y probablemente no sea aceptado!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Generado pero no aceptado</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Received with</source>
        <translation>Recibido con</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Recibidos de</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>Enviado a</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Pago proprio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>Minado</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation type="unfinished">Otra</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>de observación</translation>
    </message>
    <message>
        <location line="+276"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Estado de transacción. Pasa el ratón sobre este campo para ver el número de confirmaciones.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Fecha y hora en que se recibió la transacción.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Tipo de transacción.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Si una dirección watch-only está involucrada en esta transacción o no.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Descripción de la transacción definido por el usuario.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Cantidad retirada o añadida al saldo.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+353"/>
        <source>Exporting Failed</source>
        <translation>Exportacón Fallida</translation>
    </message>
    <message>
        <location line="-22"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Archivo separado por comas (*.csv)</translation>
    </message>
    <message>
        <location line="-175"/>
        <source>Copy transaction idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+189"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location line="-269"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Todo</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Hoy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Esta semana</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Este mes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Mes pasado</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Este año</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Rango...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Recibido con</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>Enviado a</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>A usted mismo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>Minado</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Otra</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>Introduzca una dirección o etiqueta que buscar</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Cantidad mínima</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>Copiar ubicación</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Copiar capa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copiar cantidad</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy raw transaction</source>
        <translation>Copiar transacción raw</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>Editar etiqueta</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Mostrar detalles de la transacción</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>Export Transaction History</source>
        <translation>Exportar historial de transacciones</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>Confirmado</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>De observación</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>Ha habido un error al intentar guardar la transacción con %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Exportación finalizada</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>La transacción ha sido guardada en %1.</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Range:</source>
        <translation>Rango:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>para</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+127"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>Unidad en la que se muestran las cantidades. Haga clic para seleccionar otra unidad.</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation type="unfinished">&amp;Red</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation type="unfinished">Opciones activas de consola de comandos que tienen preferencia sobre las opciones anteriores:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation type="unfinished">Restablecer todas las opciones predeterminadas del cliente.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation type="unfinished">&amp;Restablecer opciones</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;Aceptar</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation type="unfinished">Confirme el restablecimiento de las opciones</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation type="unfinished">Se necesita reiniciar el cliente para activar los cambios.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation type="unfinished">El cliente se cerrará. ¿Desea continuar?</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+31"/>
        <source>No wallet has been loaded.</source>
        <translation>No se ha cargado ningún monedero</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Token Database Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token wallet not available because a -reindex is needed. Click &quot;Ok&quot; to perform a one time -reindex on the next startup. Then shutdown Nexa-Qt and restart to complete the process.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+291"/>
        <source>Send Coins</source>
        <translation>Enviar</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+46"/>
        <location line="+20"/>
        <source>&amp;Export</source>
        <translation>&amp;Exportar</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+20"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Exporta los datos de la pestaña actual a un archivo</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Backup Wallet</source>
        <translation>Copia de seguridad del monedero</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+21"/>
        <source>Wallet Data (*.dat)</source>
        <translation>Datos de monedero (*.dat)</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Backup Failed</source>
        <translation>La copia de seguridad ha fallado</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>Ha habido un error al intentar guardar los datos del monedero en %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Backup Successful</source>
        <translation>Se ha completado con éxito la copia de respaldo</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>Los datos del monedero se han guardado con éxito en %1.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Restore Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to restore the wallet data to %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+56"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>La Poda se ha configurado por debajo del minimo de %d MiB. Por favor utiliza un valor mas alto.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>Poda:  la ultima sincronizacion de la cartera sobrepasa los datos podados. Necesitas reindexar con -reindex (o descargar la cadena de bloques de nuevo en el caso de un nodo podado)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Rescans are not possible in pruned mode. You will need to use -reindex which will download the whole blockchain again.</source>
        <translation>Nos es posible re-escanear en modo podado.Necesitas utilizar -reindex el cual descargara la cadena de bloques al completo de nuevo.</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>Un error interno fatal ocurrió, ver debug.log para detalles</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Pruning blockstore...</source>
        <translation>Poda blockstore ...</translation>
    </message>
    <message>
        <location line="-181"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation>Los %s desarrolladores</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation>No se puede bloquear el directorio %s. %s ya se está ejecutando.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation>Error cargando %s: No puede habilitar HD en un monedero existente que no es HD</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation>Error leyendo %s!. Todas las claves se han leido correctamente, pero los datos de transacciones o la libreta de direcciones pueden faltar o ser incorrectos.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation>Por favor, compruebe si la fecha y hora en su computadora son correctas! Si su reloj esta mal, %s no trabajara correctamente. </translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>La base de datos de bloques contiene un bloque que parece ser del futuro. Esto puede ser porque la fecha y hora de tu ordenador están mal ajustados. Reconstruye la base de datos de bloques solo si estas seguro de que la fecha y hora de tu ordenador estan ajustados correctamente.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>You are trying to restore the same wallet which you are trying to replace.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>You can not run &quot;-salvagewallet&quot; as an HD wallet.

Please relaunch Nexa with &quot;-usehd=0&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&quot;Restore Wallet&quot; succeeded and a backup of the previous wallet was saved to: %s.


When you click &quot;OK&quot; Nexa will shutdown to complete the process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%s corrupt, salvage failed</source>
        <translation>%s corrupto. Fracasó la recuperacion</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Corrupted block database detected</source>
        <translation>Corrupción de base de datos de bloques detectada.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>¿Quieres reconstruir la base de datos de bloques ahora?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>Error al inicializar la base de datos de bloques</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Error al inicializar el entorno de la base de datos del monedero  %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s</source>
        <translation>Error cargando %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation>Error cargando %s: Monedero dañado</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation>Error cargando %s: Monedero requiere un versión mas reciente de %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation>Error cargando %s: No puede deshabilitar HD en un monedero existente que ya es HD</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error opening block database</source>
        <translation>Error al abrir base de datos de bloques.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>Error: ¡Espacio en disco bajo!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: Keypool ran out, please call keypoolrefill first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>Ha fallado la escucha en todos los puertos. Use -listen=0 si desea esto.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>Importando...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>Incorrecto o bloque de génesis no encontrado. Datadir equivocada para la red?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation>La inicialización de la verificación de validez falló. Se está apagando %s.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds for this token.  Need %d more.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>Dirección -onion inválida: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Loading banlist...</source>
        <translation>Cargando banlist...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not enough file descriptors available.</source>
        <translation>No hay suficientes descriptores de archivo disponibles. </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Opening Token Description database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>Pode no se puede configurar con un valor negativo.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>El modo recorte es incompatible con -txindex.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation>No se ha podido conectar con %s en este equipo. %s es posible que este todavia en ejecución.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>El comentario del Agente de Usuario (%s) contiene caracteres inseguros.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation>Verificando bloques...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>Verificando monedero...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>El monedero %s se encuentra fuera del directorio de datos %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation>Es necesario reescribir el monedero: reiniciar %s para completar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zapping all transactions from wallet...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-180"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>Error: la escucha para conexiones entrantes falló (la escucha regresó el error %s)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>Monto de transacción muy pequeña luego de la deducción por comisión</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>You need to rebuild the database using -reindex to go back to unpruned mode.  This will redownload the entire blockchain</source>
        <translation>Necesitas reconstruir la base de datos utilizando -reindex para volver al modo sin recorte. Esto volverá a descargar toda la cadena de bloques</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Information</source>
        <translation>Información</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>Máscara de red inválida especificada en -whitelist: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>Necesita especificar un puerto con -whitebind: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Signing transaction failed</source>
        <translation>Transacción falló</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>Cantidad de la transacción demasiado pequeña para pagar la comisión</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>Este software es experimental.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>Cantidad de la transacción demasiado pequeña</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>Operación demasiado grande para la política de tasas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>No es posible conectar con %s en este sistema (bind ha dado el error %s)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Loading addresses...</source>
        <translation>Cargando direcciones...</translation>
    </message>
    <message>
        <location line="-71"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation>Advertencia: Se están minando versiones de bloques desconocidas! Es posible que normas desconocidas estén activas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation>Aviso: fichero de monedero corrupto, datos recuperados! Original %s guardado como %s en %s; si su balance de transacciones es incorrecto, debe restaurar desde una copia de seguridad.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>Dirección -proxy inválida: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>La red especificada en -onlynet &apos;%s&apos; es desconocida</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Loading block index...</source>
        <translation>Cargando el índice de bloques...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>Cargando monedero...</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>Cannot downgrade wallet</source>
        <translation>No se puede cambiar a una versión mas antigua el monedero</translation>
    </message>
    <message>
        <location line="-129"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%s </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Activating best chain...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot write default address</source>
        <translation>No se puede escribir la dirección predeterminada</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Opening Block database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Opening Token Mintage database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>Reexplorando...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing token transaction failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-60"/>
        <source>Done loading</source>
        <translation>Se terminó de cargar</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
</context>
</TS>
